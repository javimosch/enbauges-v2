module.exports = {
	fsId: "f3e88ff1-9126-48c6-8210-c386526a9c63",
	metaTitle:"Croc'Bauges - Marche Bio et Locaux",
	name: "Croc'Bauges",
	publicUrl:"/crocbauges",
	activity:'épicerie bio associative',
	description: `produits Bio et Locaux, consommateurs peuvent être acteurs`,
	address: {
		formatted: 'Lescheraines le Pont',
		coords: []
	},
	email: '',
	phone: '0479342352',
	type:'bon',
	relatedArticles:['epicerie-bio-croq-bauges'],
	logoImage:"https://p8.storage.canalblog.com/83/68/1260698/100052038.jpg",
};