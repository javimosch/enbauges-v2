const mongoose = require('mongoose');

const PageSectionSchema = new mongoose.Schema({
    name: {
        type:String,
        unique:true
    }, 
    type: {
        type:String,
        required: true,
        enum: ['text']
    },
    value:Object
});
const PageSection = mongoose.model('PageSection', PageSectionSchema);

const PageSchema = new mongoose.Schema({ name: 'string', size: 'string', sections: [PageSectionSchema] });
const Page = mongoose.model('Page', PageSchema);

module.exports = async app => {
    return new Promise(async (resolve, reject) => {

        mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true });
        console.log('mongo', process.env.MONGO_URI)

        mongoose.connection.once('open', function () {
            // we're connected!
            console.log('mongo ok')

            const session = require('express-session');
            const MongoStore = require('connect-mongo')(session);

            app.use(session({
                secret: process.env.SESSION_SECRET,
                saveUninitialized: true,
                resave: true,
                store: new MongoStore({ mongooseConnection: mongoose.connection })
            }));
            resolve()
        });
    })

}