

module.exports = {
	get: instanceOptions => {
		return {
			getItems: async function(options) {
				var sourcePath = options.source; //e.g blog-articles
				var path = require('path')
				var sander = require('sander')
				let files = await sander.readdir(path.join(process.cwd(), 'src', sourcePath));
				
				
				//folders will be copied root directory as it is
				var folderNames = []
				files.filter(fileName => fileName.indexOf('.') === -1).forEach(folderName => {
					folderNames.push(folderName);
				});
				
				files = files.filter(fileName => fileName.indexOf('.') !== -1);

				files = await Promise.all(files.map(fileName => {
					if (fileName.indexOf('.js') !== -1) {
						return (async() => {
							var config = require(path.join(process.cwd(), 'src', sourcePath, fileName));
							let folderName = fileName.split('.')[0]
							if(folderNames.includes(folderName)){
								await instanceOptions.copy(`src/${sourcePath}/${folderName}`, `dist/${config.name}`);
							}
							config.internalId = folderName;
							return config;
						})();
					} else {
						return (async() => {
							let raw = await sander.readFile(path.join(process.cwd(), 'src', sourcePath, fileName))
							return {
								internalId: fileName.split('.')[0],
								type: 'content',
								contents: raw.toString('utf-8')
							}
						})();
					}
				}));



				var articles = files.filter(f => f.type !== 'content')
				articles = articles.map(singleArticle => {
					var contentItem = files.find(f => f.internalId == singleArticle.internalId && f.type === 'content');
					if (contentItem) {
						singleArticle.html = contentItem.contents;
					}
					singleArticle.publicUrl = singleArticle.publicUrl || `/${singleArticle.name}`
					singleArticle.metaTitle = singleArticle.metaTitle || singleArticle.title;
					singleArticle.created_at = require('moment')(singleArticle.created_at).format('DD/MM/YYYY');
					return singleArticle;
				});
				articles = articles.filter(i => !i.draft)
				articles = articles.reverse()
				articles = articles.sort(function(a, b) {
					return (a.order && a.order || 99) <= (b.order && b.order || 99) ? -1 : 1
				})
				return articles;
			}
		}
	}
}