const { text } = require('express')
let mongoose = require('mongoose')
var m = n => mongoose.model(n)
module.exports = async app => {
    const funql = require('funql-api')
    funql.middleware(app, {
        /*defaults*/
        getMiddlewares: [],
        postMiddlewares: [],
        allowGet: true,
        allowOverwrite: false,
        attachToExpress: false,
        allowCORS: true,
        api: {
            async savePages(pages) {
                console.log(pages)
                await Promise.all(pages.map(p => {
                    return m('Page').updateOne({
                        name: p.name
                    }, {
                        $set: {
                            ...p
                        }
                    })
                }))
                await app.compile()
                return true
            },
            async loadFixtures() {

                var ensurePage = async (name, sections) => {
                    await m('Page').findOneAndUpdate({
                        name
                    }, {
                        name
                    }, {
                        upsert: true
                    })
                    let page = await m('Page').findOne({ name })
                    for (var x = 0; x < sections.length; x++) {
                        if (!page.sections.find(s => s.name == sections[x].name)) {
                            page.sections.push({
                                ...sections[x]
                            })
                            await page.save()
                        }
                    }
                    return page
                }
                
                let navbar = await ensurePage('navbar', [{
                    name: 'navbar_title',
                    type: "text",
                    value: `Bon Beau en Bauges`
                }])

                let home = await ensurePage('home', [{
                    name: 'header_message',
                    type: "text",
                    value: `Tout en bauges<br>	Bon, Beau, Local !`
                },{
                    name:"presentation_message",
                    type:'text',
                    value:`La vallee des Bauges fourmille de richesses ! Des producteurs soucieux de preserver le gout des bonnes choses tout autant que les sols et les paysages, des artisants qui perpetuent et revisitent les savoir faire traditionnels, des points de rencontre pour apprendre et partager. Nous avons hate de vous faire decouvrir tout ca au travers de ces articles. Suivez le guide !`
                }])

                let about = await ensurePage('about',[{
                    name:'about_message',
                    type:'text',
                    value:`Habitant les bauges depuis l’enfance, je suis étonnée et heureuse du dynamisme de la région depuis quelques années. Associations, artisans, producteurs respectueux de l’environnement, passionnés de nature, les bauges fourmillent de nouvelles initiatives qui font le bonheur de ses habitants comme des touristes.

                    Le bouche à bouche étant leur principale source de communication, certaines adresses ne sont pourtant pas connues des personnes des passages, des nouveaux venus ou même parfois des habitants de toujours qui n’ont pas eu vent des derniers bons plans des bauges !
                    
                    J’ai donc souhaité par ce blog apporter ma contribution au développement de cette vallée que j’affectionne tant en mettant en valeur ceux qui y vivent et produisent de façon responsable pour notre bonheur.`
                }])

                let _settings = await ensurePage('_settings',[{
                    name:"contact_email",
                    type:'text',
                    value:'cacimeteret@gmail.com'
                }])


                return [navbar, home, about, _settings];
            },
            backoffice: {
                getUsers() {
                    return ['Juan', 'Paco']
                }
            }
        }
    })
}